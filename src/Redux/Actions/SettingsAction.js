import settingConstant from "../Constant/Settings"

export const changeNationality = nat => {
  return {
    type: settingConstant.CHANGE_NATIONALITY,
    payload: nat
  }
}
