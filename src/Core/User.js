class User {
  constructor(
    userId,
    userName,
    thumbnail,
    firstName,
    lastName,
    email,
    phone,
    cell,
    street,
    state,
    city,
    postCode
  ) {
    this.userId = userId
    this.userName = userName
    this.thumbnail = thumbnail
    this.firstName = firstName
    this.lastName = lastName
    this.email = email
    this.phone = phone
    this.cell = cell
    this.street = street
    this.state = state
    this.city = city
    this.postCode = postCode
  }

  static createFromObject(obj) {
    return new User(
      obj.login.uuid,
      obj.login.userName,
      obj.picture.thumbnail,
      obj.name.first,
      obj.name.last,
      obj.email,
      obj.phone,
      obj.cell,
      obj.location.street,
      obj.location.state,
      obj.location.city,
      obj.location.postCode
    )
  }
}
export default User
