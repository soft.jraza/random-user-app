import * as JsSearch from "js-search"

class SearchService {
  constructor(users) {
    this.SearchIndex(users)
  }

  SearchIndex(users) {
    const data = new JsSearch.Search("isbn")
    data.indexStrategy = new JsSearch.PrefixIndexStrategy()
    data.sanitizer = new JsSearch.LowerCaseSanitizer()
    data.searchIndex = new JsSearch.TfIdfSearchIndex("isbn")
    data.addIndex("firstName")
    data.addIndex("lastName")
    data.addDocuments(users)
    this.dataSearch = data
  }

  SearchByUserName(users) {
    return this.dataSearch.search(users)
  }
}

export default SearchService
