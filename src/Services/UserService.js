//import axios from "axios"
import User from "../Core/User"
import Pagination from "../Core/Pagination"

const SEED = "joanr"
const PER_PAGE = 50

 class UserService {
  static async getUsers(
     pageNo,
    nationalities = ["CH", "ES", "FR", "GB"],
    limit = PER_PAGE,
    fields = "name,email,login,picture,location,cell,phone,nat"
  ) {
    const res = await fetch(
      `https://randomuser.me/api/?results=${limit}&page=${pageNo}&nat=${nationalities}&inc=${fields}&seed=${SEED}`
    )
    return {
      users: res.data.results.map(user => {
        return User.createFromObject(user)
      }),
      paginationInfo: Pagination.createPagination(res.data.info.page)
    }
  }
}
export default UserService