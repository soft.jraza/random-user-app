import React, { Component } from "react"
import { Layout } from "antd"

const { Footer } = Layout

class AppFooter extends Component {
  render() {
    return (
      <Footer
        style={{
          textAlign: "center",
          background: "#011529",
          color: "#ffffff"
        }}>
        Address Book © 2020 Created with{" "}
        <a href='https://reactjs.org/'>React.js</a>
        <a href='https://ant.design/'>Ant Design</a>
      </Footer>
    )
  }
}

export default AppFooter
