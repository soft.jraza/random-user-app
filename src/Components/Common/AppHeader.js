import React, { Component } from "react"
import { NavLink, withRouter } from "react-router-dom"
import { Layout, Menu, Icon } from "antd"

const { Header } = Layout

class AppHeader extends Component {
  render() {
    const { location } = this.props
    return (
      <Header>
        <div className='header-logo' />
        <Menu
          theme='dark'
          mode='horizontal'
          defaultSelectedKeys={["/"]}
          selectedKeys={[location.pathname]}
          style={{ lineHeight: "72px" }}>
          <Menu.Item>
            <span>Address Book App</span>
          </Menu.Item>
          <Menu.Item key='/home'>
            <NavLink to='/home'>
              <Icon type='home' />
              <span>Home</span>
            </NavLink>
          </Menu.Item>
          <Menu.Item key='/settings'>
            <NavLink to='/settings'>
              <Icon type='setting' />
              <span>Settings</span>
            </NavLink>
          </Menu.Item>
        </Menu>
      </Header>
    )
  }
}

export default withRouter(AppHeader)
