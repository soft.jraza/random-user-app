import React, { Component } from "react"
import { Input } from "antd"

class UserSearch extends Component {
  render() {
    return (
      <Input.Search
        placeholder='Search by first and last name'
        enterButton
        allowClear={true}
        onSearch={this.props.onSearch}
      />
    )
  }
}

export default UserSearch
