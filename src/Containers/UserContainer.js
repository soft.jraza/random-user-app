import React, { Component } from "react"
import { connect } from "react-redux"
import { Row, Col, Affix } from "antd"

import UserGrid from "../Components/User/UserGrid"
import UserDetailModal from "../Components/User/UserModal"
import UserSearch from "../Components/User/UserSearch"

import {
  getUsersData,
  showUserDetail,
  closeUserDetail,
  searchUser
} from "../Redux/Actions/UserAction"

class UserContainer extends Component {
  
  componentDidMount() {
    this.props.getUserData()
    this.handleScroll()
  }

  handleScroll() {
    window.onscroll = async () => {
      if (this.props.user.isLoading || !this.props.paginationInfo.hasMore())
        return
      if (
        window.innerHeight + document.documentElement.scrollTop ===
        document.documentElement.scrollHeight
      ) {
        await this.props.getUserData
      }
    }
  }

  render() {
    return (
      <Row>
        <col>
          <Row>
            <Col span={6}>
              <Affix style={{ marginTop: "10px" }}>
                <UserSearch onSearch={this.props.handleSearch} />
              </Affix>
            </Col>
          </Row>
          <Row>
            <UserDetailModal
                user={this.props.userDetail}
                onClose={this.props.closeUserDetail}
              />
          </Row>
          <Row>
            <hr />
            <UserGrid
              user={this.props.filteredUsers}
              isLoading={this.props.isLoading}
              onUserClick={this.props.showUserDetail}
              hasMore={this.props.paginationInfo}
            />
          </Row>
        </col>
      </Row>
    )
  }
}

const mapStateToProps = state => {
return {
  user: state.user
 }
}
const mapDispatchToProps = dispatch => {
  return {
    getUserData: () => dispatch(getUsersData()),
    showUserDetail: data => dispatch(showUserDetail(data)),
    closeUserDetail: () => dispatch(closeUserDetail()),
    handleSearch: value => dispatch(searchUser(value))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserContainer)
