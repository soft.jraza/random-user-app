import React, { Component } from "react"
import { BrowserRouter as Router, Route } from "react-router-dom"
import { Provider } from "react-redux"
import store from "./Redux/Store/store"

import AppHeader from "./Components/Common/AppHeader"
import AppFooter from "./Components/Common/AppFooter"
import SettingContainer from "./Containers/SettingsContainer"
import UserContainer from "./Containers/UserContainer"

import { Layout } from "antd"
const { Content } = Layout

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Layout>
            <AppHeader />
            <Content
              style={{
                background: "##DAF7A6 ",
                height: "100%",
                padding: "0 85px"
              }}>
              <Route exact path='/home' component={UserContainer} />
              <Route exact path='/setting' component={SettingContainer} />
            </Content>
            <AppFooter />
          </Layout>
        </Router>
      </Provider>
    )
  }
}

export default App
